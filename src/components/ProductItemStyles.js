import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  text: {
    fontSize: 14,
    textAlign: 'left',
    padding: 10,
    height: 44
  },
  icon: {
    height: 20,
    width: 20
  }
})
