
import React from 'react'
import { Text, View, Image, TouchableOpacity } from 'react-native'
import { title } from '../types/Product'
import { styles } from './ProductItemStyles'

const ProductItem = ({ product, onPress }) => {
  return (
    <View style={styles.container}>
      <Image
        style={styles.icon}
        source={require('../../assets/images/arrow.png')}
      />
      <TouchableOpacity onPress={() => onPress(product)}>
        <Text style={styles.text}>
          { title(product) }
        </Text>
      </TouchableOpacity>
    </View>
  )
}

export default ProductItem
