const BASE_URL = 'http://ecsc00a02fb3.epam.com/rest/V1/'

const LOGIN_PATH = 'integration/customer/token'
const PRODUCTS_PATH = 'products'

const SEARCH_CRITERIA_PARAM = 'searchCriteria'
const PAGE_SIZE_KEY = 'pageSize'
const CURRENT_PAGE_KEY = 'currentPage'

const encodeParams = ({ currentPage, pageSize }) => {
  const params = [
    encodeParam({
      param: SEARCH_CRITERIA_PARAM,
      key: PAGE_SIZE_KEY,
      value: pageSize
    }),
    encodeParam({
      param: SEARCH_CRITERIA_PARAM,
      key: CURRENT_PAGE_KEY,
      value: currentPage
    })
  ]
  return params.join('&')
}

const encodeParam = ({ param, key, value }) => {
  const encodedValue = encodeURIComponent(value)
  return `${param}[${key}]=${encodedValue}`
}

export const makeRequest = (url, params) => {
  const headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }

  return fetch(url, {
    headers,
    ...params
  })
}

export const makeLogin = async ({ username, password }) => {
  const body = JSON.stringify({ username, password })

  const url = BASE_URL + LOGIN_PATH

  let success
  try {
    const response = await makeRequest(url, {
      method: 'POST',
      body
    })
    success = response.status === 200
  } catch (e) {
    success = false
  }

  return success
}

const PAGE_SIZE = 15

export const fetchProducts = async ({ page = 0, size = PAGE_SIZE } = {}) => {
  const query = encodeParams({ currentPage: page, pageSize: size })
  const url = BASE_URL + `${PRODUCTS_PATH}?${query}`

  const response = await makeRequest(url)

  const json = await response.json()
  const products = json.items
  const totalCount = json.total_count

  return { value: products, totalCount: totalCount }
}
