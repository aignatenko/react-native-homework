/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import { createStackNavigator, createAppContainer } from 'react-navigation'

import LoginScreen from './screens/LoginScreen'
import ProductScreen from './screens/ProductScreen'
import ProductListScreen from './screens/ProductListScreen'

const AppNavigator = createStackNavigator(
  {
    Login: {
      screen: LoginScreen
    },
    Product: {
      screen: ProductScreen
    },
    Products: {
      screen: ProductListScreen
    }
  },
  {
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#8bc83d'
      },
      headerTintColor: '#222222'
    }
  }
)

export default createAppContainer(AppNavigator)
