export const products = [
  {
    id: '1',
    name: 'Book'
  },
  {
    id: '2',
    name: 'Car'
  },
  {
    id: '3',
    name: 'Notebook'
  },
  {
    id: '4',
    name: 'Phone'
  },
  {
    id: '5',
    name: 'Watch'
  }
]
