import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  },
  title: {
    fontFamily: 'vincHand',
    fontSize: 50,
    textAlign: 'center',
    marginTop: 10,
    marginBottom: 10
  },
  body: {
    fontSize: 14,
    textAlign: 'justify',
    marginLeft: 40,
    marginRight: 40
  }
})
