// @flow

import React, { Component } from 'react'
import {
  ActivityIndicator,
  Modal,
  Button,
  Text,
  TextInput,
  View,
  Animated,
  Easing
} from 'react-native'
import { styles } from './LoginScreenStyles'
import * as network from '../network'

type Props = {
  navigation: any
}

type State = {
  isLoading: bool,
  isErrorVisible: bool,
  email: string,
  password: string
}

export default class LoginScreen extends Component<Props, State> {
  static navigationOptions = {
    title: 'Login'
  }

  state = { isLoading: false, isErrorVisible: false, email: '', password: '', fadeAnim: new Animated.Value(1) }

  onEmailChange = (text: string): void => {
    this.setState({ email: text })
  }

  onPasswordChange = (text: string): void => {
    this.setState({ password: text })
  }

  animateHeader = (callback=() => {}): void => {
    Animated.timing(
      this.state.fadeAnim,
      {
        toValue: 0,
        duration: 1000,
        easing: Easing.back(5),
        useNativeDriver: true,
      }
    ).start(() => {
      callback()
    })
  }

  resetAnimation = () => {
    const didBlurSubscription = this.props.navigation.addListener(
      'didBlur',
      payload => {
        this.setState({fadeAnim: new Animated.Value(1)})
        didBlurSubscription.remove()
      }
    );
  }

  onLogin = async (): void => {
    const { email: username, password } = this.state

    this.setState({ isLoading: true })
    const success = await network.makeLogin({ username, password })
    this.setState({ isLoading: false })

    if (success) {
      this.animateHeader(() => {
          this.props.navigation.navigate('Products')
      })
    } else {
      this.setState({ isErrorVisible: true })
    }

    this.resetAnimation()
  }

  onDismiss = (): void => {
    this.setState({ isErrorVisible: false })
  }

  onTryAgain = async (): void => {
    this.setState({ isErrorVisible: false })
    this.onLogin()
  }

  render () {
    const { isLoading, isErrorVisible, fadeAnim } = this.state
    return (
      <View style={styles.container}>
        <Modal
          animationType='slide'
          transparent={false}
          visible={isErrorVisible}
          onRequestClose={this.onDismiss}
        >
          <View style={styles.container}>
            <Text style={styles.errorTitle}>Wrong email or password</Text>
            <Text style={styles.errorMessage}>Entered email or password is not correct, please try again</Text>
            <Button
              onPress={this.onDismiss}
              title='Dismiss'
            />
            <Button
              onPress={this.onTryAgain}
              title='Try Again'
            />
          </View>
        </Modal>

        <Animated.View style={{
          opacity: fadeAnim,
          transform: [{
            translateX: fadeAnim.interpolate({
              inputRange: [0, 1],
              outputRange: [-300, 0]
            })
          }]
        }}>
          <Text style={styles.icon}>
            😀
          </Text>
        </Animated.View>
        <Animated.View style={{
          opacity: fadeAnim,
          transform: [{
            translateX: fadeAnim.interpolate({
              inputRange: [0, 1],
              outputRange: [300, 0],
            })
          }]
        }}>
          <Text style={styles.welcome}>
            Friday's Shop
          </Text>
        </Animated.View>
        <TextInput
          style={styles.field}
          textContentType='emailAddress'
          placeholder='email'
          autoCapitalize='none'
          autoFocus
          onChangeText={this.onEmailChange}
          value={this.state.email}
        />
        <TextInput
          style={styles.field}
          placeholder='password'
          textContentType='password'
          secureTextEntry
          autoCapitalize='none'
          onChangeText={this.onPasswordChange}
          value={this.state.password}
        />
        <View style={styles.buttonContainer}>
          { isLoading ? (<ActivityIndicator style={styles.activityIndicator} />) : null }
          <Button
            onPress={this.onLogin}
            title='login'
          />
        </View>
      </View>
    )
  }
}
