import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  },
  icon: {
    fontSize: 40,
    textAlign: 'center',
    margin: 10
  },
  welcome: {
    fontFamily: 'vincHand',
    fontSize: 50,
    textAlign: 'center',
    marginTop: 10,
    marginBottom: 40
  },
  field: {
    fontSize: 20,
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5
  },
  buttonContainer: {
    flexDirection: 'row',
    margin: 30
  },
  activityIndicator: {
    position: 'absolute',
    left: -20,
    top: 10
  },
  errorTitle: {
    fontWeight: 'bold',
    fontSize: 20,
    padding: 10
  },
  errorMessage: {
    padding: 10
  }
})
