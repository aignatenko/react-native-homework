
import React, { Component } from 'react'
import { FlatList, Text, View } from 'react-native'
import ProductItem from '../components/ProductItem'
import { styles } from './ProductListScreenStyles'
import * as network from '../network'
import * as Product from '../types/Product'

const ProductListHeader = () => {
  return (
    <Text style={styles.title}>
      Products
    </Text>
  )
}

class ProductListScreen extends Component {
  state = { isRefreshing: false, products: [], currentPage: 1, isLastPage: false }

  onRefresh = async () => {
    await this.refreshProducts()
  }

  refreshProducts = async () => {
    this.setState({ isRefreshing: true, currentPage: 1 })
    const { value: products } = await network.fetchProducts()
    this.setState({
      products,
      isRefreshing: false,
      isLastPage: false
    })
  }

  fetchNextPage = async () => {
    if (this.state.isLastPage) {
      return
    }

    const { currentPage, products } = this.state
    const nextPage = currentPage + 1
    const { value: nextPageProducts, totalCount } = await network.fetchProducts({ page: nextPage })

    const allProducts = [...products, ...nextPageProducts]
    const isLastPage = allProducts.length === totalCount

    this.setState({
      isLastPage,
      currentPage: nextPage,
      products: allProducts
    })
  }

  onEndReached = () => {
    this.fetchNextPage()
  }

  async componentDidMount () {
    await this.refreshProducts()
  }

  render () {
    const { isRefreshing, products } = this.state
    const { navigation } = this.props

    const showProduct = (product) => {
      navigation.navigate('Product', {
        product
      })
    }

    return (
      <View style={styles.container}>
        <FlatList
          data={products}
          keyExtractor={(product, index) => Product.id(product)}
          renderItem={({ item: product }) => <ProductItem product={product} onPress={showProduct} />}
          ListHeaderComponent={ProductListHeader}
          onRefresh={this.onRefresh}
          refreshing={isRefreshing}
          onEndReachedThreshold={0.01}
          onEndReached={this.onEndReached}
        />
      </View>
    )
  }
}

ProductListScreen.navigationOptions = {
  title: 'Products'
}

export default ProductListScreen
