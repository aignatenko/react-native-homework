
import React from 'react'
import { Button, Text, View } from 'react-native'
import { title, body } from '../types/Product'
import { styles } from './ProductScreenStyles'

const ProductScreen = ({ product, navigation }) => {
  product = product || navigation.getParam('product')
  return (
    <View style={styles.container}>
      <Text style={styles.title}>
        { title(product) }
      </Text>
      <Text style={styles.body}>
        { body(product) }
      </Text>
      <Button
        onPress={() => navigation.goBack()}
        title='All Products'
      />
    </View>
  )
}

ProductScreen.navigationOptions = ({ navigation }) => {
  const product = navigation.getParam('product')
  return ({
    title: title(product)
  })
}

export default ProductScreen
