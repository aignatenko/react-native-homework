import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 66,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: '#f5fcff'
  },
  title: {
    fontSize: 50,
    fontFamily: 'vincHand'
  }
})
