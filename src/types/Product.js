export const id = (product) => {
  return product.id + ''
}

export const title = (product) => {
  return product.name
}

export const body = (product) => {
  return `
  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas porttitor blandit pharetra. Sed malesuada varius magna. Donec in turpis erat. Nam sollicitudin turpis magna, in laoreet nibh luctus at. Aenean ullamcorper, lorem commodo tincidunt suscipit, diam lorem iaculis urna, sed dapibus sapien massa sed nunc. Vestibulum venenatis dolor ut tortor vulputate, vitae dignissim sapien scelerisque. Phasellus ante nisi, semper eu ultrices in, interdum non odio. Maecenas efficitur erat vel nulla eleifend volutpat.
  `
}
